package pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.database;

import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core.ExpenseGroup;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core.User;

import java.util.*;

/**
 * A singleton that locally simulates interactions with a database.
 * Should be used for testing purposes only.
 */
public class TestDatabaseManager extends DatabaseManager {

    private final static TestDatabaseManager instance = new TestDatabaseManager();
    private final Map<String, User> users = new HashMap<>();
    private final Map<String, ExpenseGroup> groups = new HashMap<>();

    private TestDatabaseManager() {
    }

    public static TestDatabaseManager getInstance() {
        return instance;
    }

    /**
     * Adds desired number of users with randomised emails and names to the
     * database.
     *
     * @param count number of users to add
     */
    public void addRandomUsers(int count) {
        Random rand = new Random();
        for (int i = 0; i < count; i++) {
            String name = generateRandomTitle(rand.nextInt(4) + 4);
            String email = name.toLowerCase() + "@gmail.com";
            users.put(email, new User(name, email));
        }
    }

    @Override
    public User signUp(String email, String password) {
        if (users.containsKey(email)) {
            return null;
        }
        String usernameFromEmail = generateUsernameFromEmail(email);
        String username = usernameFromEmail;
        int num = 1;
        while (users.containsKey(username)) {
            username = usernameFromEmail + num;
            num++;
        }
        User user = new User(email, username);
        users.put(username, user);
        return user;
    }

    @Override
    public User signIn(String email, String password) {
        String username = users.keySet().stream()
                .filter(key -> users.get(key).getMail().equals(email))
                .findFirst()
                .orElse(null);
        if (username == null) {
            return null;
        }
        return users.get(username);
    }

    @Override
    public void signOut() {
    }

    @Override
    public User getUser(String name) {
        return users.getOrDefault(name, null);
    }

    @Override
    public void storeUser(User user) {
        users.put(user.getName(), user);
    }

    @Override
    public boolean updateUsername(User user, String newName) {
        if (users.containsKey(newName) || !users.containsKey(user.getName())) {
            return false;
        }
        users.remove(user.getName());
        user.setName(newName);
        users.put(newName, user);
        return true;
    }

    @Override
    public List<User> getAllUsers() {
        return new ArrayList<>(users.values());
    }

    @Override
    public ExpenseGroup getExpenseGroup(String groupName) {
        return groups.getOrDefault(groupName, null);
    }

    @Override
    public void storeExpenseGroup(ExpenseGroup expenseGroup) {
        groups.put(expenseGroup.getName(), expenseGroup);
    }

    @Override
    public void deleteExpenseGroup(ExpenseGroup expenseGroup) {
        groups.remove(expenseGroup.getName());
        users.keySet().forEach(username -> users.get(username)
                .getGroupsNames().remove(expenseGroup.getName()));
    }

    @Override
    public List<ExpenseGroup> getAllExpenseGroups() {
        return new ArrayList<>(groups.values());
    }

    /**
     * Generates a random string of a given length with the first letter
     * capitalised, and the rest left lowercase.
     *
     * @param length desired string length
     * @return String with random characters
     */
    public String generateRandomTitle(int length) {
        Random rand = new Random();
        char[] chars = new char[length];
        for (int i = 0; i < length; i++) {
            chars[i] = (char) (rand.nextInt(26) + 97);
        }
        // 1st letter capitalised
        chars[0] = (char) (chars[0] - 32);
        return String.valueOf(chars);
    }
}
