package pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.database;

/**
 * Stores most common Firebase Auth REST API error types
 * @see FirebaseHttpException
 */
public enum FirebaseHttpExceptionType {

    // sign up errors:
    EMAIL_EXISTS,
    TOO_MANY_ATTEMPTS_TRY_LATER,
    WEAK_PASSWORD,

    // sign in errors:
    EMAIL_NOT_FOUND,
    INVALID_PASSWORD,
    USER_DISABLED,

    // applies to both:
    INVALID_EMAIL,
    MISSING_PASSWORD,
    UNKNOWN_ERROR;
}
