package pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core;

import java.util.*;

/**
 * A class used for optimising group costs being given {@code totalUsersExpenseMap}
 */
public class Optimizer {

    /**
     * A map containing total expense incurred by each user
     */
    private HashMap<User, Double> totalUsersExpenseMap;

    /**
     * A balance map (the amount I have paid minus the amount others have paid for me)
     */
    private HashMap<User, Double> balanceMap;

    /**
     * An array list used to display the information whether the group is settled up, etc.
     */
    private ArrayList<String> optimiseInstructions;

    /**
     * An array list containing names of all users in particular group
     */
    private ArrayList<User> key;

    /**
     * The constructor for Optimizer class
     *
     * @param usersBalancesMap
     */
    public Optimizer(HashMap<User, Double> usersBalancesMap) {
        totalUsersExpenseMap = usersBalancesMap;
        balanceMap = new HashMap<>();
        optimiseInstructions = new ArrayList<>();
        key = new ArrayList<>(usersBalancesMap.keySet());
    }

    /**
     * A utility function that returns index of minimum value in array
     *
     * @param arr Array with values of double type
     * @return index of minimum value in arr
     */
    private int getMin(double arr[]) {
        int minInd = 0;
        for (int i = 1; i < totalUsersExpenseMap.size(); i++)
            if (arr[i] < arr[minInd])
                minInd = i;
        return minInd;
    }

    /**
     * A utility function that returns index of maximum value in array
     *
     * @param arr Array with values of double type
     * @return index of maximum value in arr
     */
    private int getMax(double arr[]) {
        int maxInd = 0;
        for (int i = 1; i < totalUsersExpenseMap.size(); i++)
            if (arr[i] > arr[maxInd])
                maxInd = i;
        return maxInd;
    }

    /**
     * A utility function that returns minimum of 2 values
     *
     * @return min(x, y)
     */
    private double minOf2(double x, double y) {
        return (x < y) ? x : y;
    }

    /**
     * A recursive method used for settling the debts by finding
     * the indexes of minimum and maximum values in {@code amount[]}.
     * The recursion terminates when either the maximum amount to
     * be given to any person or the maximum amount to be taken
     * from any person becomes 0.
     *
     * @param amount {@code amount[p]} indicates the net amount
     *               to be credited/debited to/from person 'p'
     *               if {@code amount[p]} is positive, then
     *               i'th person will get {@code amount[i]}
     *               If {@code amount[p]} is negative, then
     *               i'th person will give {@code -amount[i]}
     */
    private void minCashFlowRec(double amount[]) {
        // Find the indexes of minimum and
        // maximum values in amount[]
        // amount[mxCredit] indicates the maximum amount
        // to be given (or credited) to any person .
        // And amount[mxDebit] indicates the maximum amount
        // to be taken(or debited) from any person.
        // So if there is a positive value in amount[],
        // then there must be a negative value
        int mxCredit = getMax(amount), mxDebit = getMin(amount);

        // Find the minimum of two amounts
        double min = minOf2(-amount[mxDebit], amount[mxCredit]);
        amount[mxCredit] -= min;
        amount[mxDebit] += min;

        // If minimum is the maximum amount to
        if (min != 0.0) {
            optimiseInstructions.add(key.get(mxDebit).getName() + " pays " + Math.round(min * 100.0) / 100.0
                    + " PLN to " + key.get(mxCredit).getName() + ".");
        }

        // If both amounts are 0, then
        // all amounts are settled
        if (Math.round(amount[mxCredit] * 100.0) / 100.0 == 0.0 && Math.round(amount[mxDebit] * 100.0) / 100.0 == 0.0) {
            //optimiseInstructions.add("Everyone is settled up.");
            return;
        }

        // Recur for the amount array.
        // Note that it is guaranteed that
        // the recursion would terminate
        // as either amount[mxCredit]  or
        // amount[mxDebit] becomes 0
        minCashFlowRec(amount);
    }

    /**
     * Given a set of persons as graph[], this function
     * finds and prints the minimum
     * cash flow to settle all debts.
     *
     * @param graph {@code graph[i][j]} indicates
     *              the amount that person i needs to
     *              pay person j
     */
    private void minCashFlow(double graph[][]) {
        // Create an array amount[],
        // initialize all value in it as 0.
        int n = totalUsersExpenseMap.size();
        double amount[] = new double[n];

        // Calculate the net amount to
        // be paid to person 'p', and
        // stores it in amount[p]. The
        // value of amount[p] can be
        // calculated by subtracting
        // debts of 'p' from credits of 'p'
        for (int p = 0; p < n; p++) {
            for (int i = 0; i < n; i++) {
                amount[p] += (graph[i][p] - graph[p][i]);
            }
            balanceMap.put(key.get(p), Math.round(amount[p] * 100.0) / 100.0);
        }
        double[] zeroArr = new double[n];
        if (Arrays.equals(amount, zeroArr)) {
            optimiseInstructions.add("Everyone is settled up.");
        } else {
            for (int j = 0; j < n; j++) {
                if (Math.round(amount[j] * 100.0) / 100.0 == 0.0) {
                    optimiseInstructions.add(key.get(j).getName() + " is settled up.");
                }
            }
        }
        minCashFlowRec(amount);
    }

    /**
     * A method for the total group cost optimisation. It creates a nxn matrix (where n is simply
     * the number of group members) containing the amounts the users owe each other. Then the
     * {@code minCashFlow} method is used.
     */
    public void optimise() {
        int n = totalUsersExpenseMap.size();
        double[][] graph = new double[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    graph[i][j] = 0.0;
                } else {
                    graph[i][j] = totalUsersExpenseMap.get(key.get(j)) / n;
                }
            }
        }
        minCashFlow(graph);
    }

    public HashMap<User, Double> getBalanceMap() {
        return balanceMap;
    }

    public ArrayList<String> getOptimiseInstructions() {
        return optimiseInstructions;
    }
}
