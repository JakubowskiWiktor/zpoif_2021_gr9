package pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.database;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.http.HttpResponse;

/**
 * Represents an error returned as a response from Firebase Auth REST API
 * @see AppFirebaseManager
 * @see FirebaseHttpExceptionType
 */
public class FirebaseHttpException extends IOException {

    /**
     * HTTP error code. -1 if code couldn't be retrieved
     */
    private final int errorCode;

    /**
     * Type of the error
     */
    private final FirebaseHttpExceptionType errorType;

    /**
     * Error description
     */
    private final String errorDescription;

    /**
     * Creates a new {@link FirebaseHttpException} from the response from
     * Firebase Auth REST API
     *
     * @param response JSON-like HTTP response from API
     */
    public FirebaseHttpException(HttpResponse<String> response) {
        super(response.body());
        String responseBody = response.body();
        JSONObject errorBody;

        try {
            errorBody = new JSONObject(responseBody).getJSONObject("error");
        } catch (JSONException err) {
            // when the format of an error is invalid
            this.errorCode = -1;
            this.errorType = FirebaseHttpExceptionType.UNKNOWN_ERROR;
            this.errorDescription = responseBody;
            return;
        }

        int errorCode = errorBody.getInt("code");
        FirebaseHttpExceptionType errorType;
        String[] message = errorBody.getString("message").split(" : ");
        try {
            errorType = FirebaseHttpExceptionType.valueOf(message[0]);
        } catch (IllegalArgumentException err) {
            errorType = FirebaseHttpExceptionType.UNKNOWN_ERROR;
        }
        this.errorCode = errorCode;
        this.errorType = errorType;
        this.errorDescription = message[message.length - 1];
    }

    public int getErrorCode() {
        return errorCode;
    }

    public FirebaseHttpExceptionType getErrorType() {
        return errorType;
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}
