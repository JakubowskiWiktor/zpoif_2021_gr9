package pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core.Expense;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core.ExpenseGroup;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core.Optimizer;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core.User;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.database.AppFirebaseManager;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.database.DatabaseManager;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * A controller for group menu screen
 *
 * @see User
 * @see ExpenseGroup
 * @see DatabaseManager
 * @see AppFirebaseManager
 */
public class GroupMenuController {

    /**
     * An instance of {@link AppFirebaseManager} used to interact
     * with the database
     */
    private final DatabaseManager database = AppFirebaseManager.getInstance();

    /**
     * An {@link ExpenseGroup} object representing currently viewed group
     */
    private ExpenseGroup expenseGroup;

    /**
     * A {@link User} object representing the user that is viewing this screen
     */
    private User user;

    @FXML
    private Parent root;

    /**
     * A ComboBox that lets user select a new contributor when
     * adding a new expense
     */
    @FXML
    private ComboBox<User> contributorPerson;

    /**
     * A ListView that can either display users' balances, their total
     * spendings or expenses history
     * (the one on the left-top of the screen).
     */
    @FXML
    private ListView<String> balanceAndHistoryListView;

    /**
     * A ListView displaying details of a new expense that is being added
     * (the one on the right of the screen).
     */
    @FXML
    private ListView<String> expenseListView;

    /**
     * A ListView that can either display optimalisation results or details
     * of the expense selected from expenses history
     * (the one on the left-bottom of the screen).
     */
    @FXML
    private ListView<String> optimalizationAndExpenseDetailsListView;

    /**
     * A text field where user can type the size of a contribution that he/she
     * wants to add to the expense that is being created
     */
    @FXML
    private TextField expenseAmount;

    /**
     * A text field where user can type the name of
     * the expense that is being created
     */
    @FXML
    private TextField expenseName;

    /**
     * A label displayed above the {@code balanceAndHistoryListView} that can
     * either display "Users balances", "Users total spendings" or
     * "Transaction history" (depending on what the
     * {@code balanceAndHistoryListView} is currently displaying)
     */
    @FXML
    private Label balanceAndHistoryLabel;

    /**
     * A label displayed above the {@code expenseListView} that can
     * either display "Expense details" or "Optimalisation"
     * (depending on what the {@code expenseListView} is currently displaying)
     */
    @FXML
    private Label optimalizationLabel;

    /**
     * A label that displays the name of the currently viewed group
     */
    @FXML
    private Label groupNameLabel;

    /**
     * A map that stores contributions of the expense that is currently
     * being created.
     */
    private Map<User, Double> expense = new HashMap<>();

    /**
     * A value indicating what is currently being displayed in the
     * {@code balanceAndHistoryListView}
     */
    private String sceneryDisplayed = "balances";

    /**
     * Initialises some key fields of this class
     *
     * @param expenseGroup A group which details are shown on this screen
     * @param user         Currently logged-in user
     */
    public void setParameters(ExpenseGroup expenseGroup, User user) {
        this.expenseGroup = expenseGroup;
        this.user = database.getUser(user.getName());  // to get a fresh version of this user
        updateBalancesAndHistoryListView();
        viewOptimalization();
    }

    /**
     * Updates the text of the {@code groupNameLabel}
     *
     * @param groupName new text to be displayed
     */
    public void setGroupName(String groupName) {
        this.groupNameLabel.setText(groupName);
    }

    /**
     * Makes {@code balanceAndHistoryListView} display users' balances.
     */
    @FXML
    protected void viewBalance() {
        sceneryDisplayed = "balances";
        updateBalancesAndHistoryListView();

    }

    /**
     * Makes {@code balanceAndHistoryListView} display transaction history.
     */
    @FXML
    protected void viewTransactionHistory(ActionEvent event) {
        sceneryDisplayed = "history";
        updateBalancesAndHistoryListView();
    }

    /**
     * Makes {@code optimalizationAndExpenseDetailsListView} display the
     * result of expense optimialisation (shows how much some users have to
     * pay other users).
     */
    @FXML
    protected void viewOptimalization() {
        Optimizer optimizer = new Optimizer(expenseGroup.getTotalUsersExpenseMap());
        optimizer.optimise();
        optimalizationLabel.setText("Optimalization");
        optimalizationAndExpenseDetailsListView.setItems(
                FXCollections.observableArrayList(optimizer.getOptimiseInstructions())); // pokazanie instrukcji do optymalizacji
        optimalizationAndExpenseDetailsListView.setVisible(true);
        expenseGroup.setUsersBalancesMap(optimizer.getBalanceMap());
        database.storeExpenseGroup(expenseGroup);
    }

    /**
     * Makes {@code balanceAndHistoryListView} display users' total spendings
     */
    @FXML
    protected void viewUsersTotalSpendings() {
        sceneryDisplayed = "total spendings";
        updateBalancesAndHistoryListView();
    }

    /**
     * Permanently deletes the group which details are being displayed
     * on this screen.
     */
    @FXML
    protected void deleteGroup(ActionEvent event) throws IOException {
        Alert confirmation = new Alert(Alert.AlertType.WARNING, "", ButtonType.OK, ButtonType.CANCEL);
        confirmation.setTitle("Warning");
        confirmation.setHeaderText("Are you sure to delete this group? " +
                "This operation cannot be undone.");
        Optional<ButtonType> buttonPressed = confirmation.showAndWait();
        if (buttonPressed.isPresent() &&
                buttonPressed.get() == ButtonType.OK) {
            database.deleteExpenseGroup(expenseGroup);
            goBackToMainMenu(event);
        }
    }

    /**
     * Switches screen to main menu.
     *
     * @see MainMenuController
     */
    @FXML
    protected void goBackToMainMenu(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass()
                .getResource("/pl/edu/pw/mini/wjzkmo/dziel_i_rzadz/main-menu.fxml"));
        root = loader.load();
        Stage stage = (Stage) (groupNameLabel).getScene().getWindow();
        Scene scene = new Scene(root);
        MainMenuController mainMenuController = loader.getController();
        mainMenuController.setUser(user);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Updates the {@code contributorPerson} ComboBox's items
     */
    @FXML
    protected void selectPersonWhoPaid() {
        contributorPerson.setItems(FXCollections.observableArrayList(expenseGroup.getUsers()));
    }

    /**
     * Finalises the process of adding a contribution to the expense that
     * is currently being created.
     */
    @FXML
    protected void addContribution(ActionEvent event) {

        User contributor = contributorPerson.getValue();
        if (contributorPerson.getValue() == null) {
            Alert invalidNameAlert = new Alert(Alert.AlertType.ERROR);
            invalidNameAlert.setTitle("Invalid input");
            invalidNameAlert.setHeaderText("Please select contributor.");
            invalidNameAlert.show();
            return;
        }
        if (!(expenseAmount.getText().matches("\\d+(\\.\\d{1,2})?"))) {  // invalid input (not double)
            Alert invalidNameAlert = new Alert(Alert.AlertType.ERROR);
            invalidNameAlert.setTitle("Invalid input");
            invalidNameAlert.setHeaderText("Expense amount should be type double with maximum of 2 decimal places.");
            invalidNameAlert.show();
            return;
        }
        Double amount = Double.parseDouble(expenseAmount.getText());
        List<String> expenseList = expenseListView.getItems();
        expenseList.add(contributor.getName() + ": " + amount + " PLN.");
        expenseListView.setItems(FXCollections.observableArrayList(expenseList));
        expense.put(contributor, amount);

    }

    /**
     * Finalises the process of adding a new expense.
     */
    @FXML
    protected void finalizeExpense() {

        ArrayList<String> expenseNames = new ArrayList<>();
        for (Expense e : expenseGroup.getExpenseList()) {
            expenseNames.add(e.getName());
        }

        if (expenseName.getText().matches("[A-Za-z0-9_ąężźćółńś]+") && !(expenseNames.contains(expenseName.getText()))) {
            expenseListView.setItems(FXCollections.observableArrayList(new ArrayList<>()));
            expenseGroup.getExpenseList().add(new Expense(expenseName.getText(), new HashMap<>(expense)));
            database.storeExpenseGroup(expenseGroup);
            for (User u : expense.keySet()) {
                Double number = expenseGroup.getTotalUsersExpenseMap().get(u) + expense.get(u);
                expenseGroup.getTotalUsersExpenseMap().put(u, number);
            }
            expense.clear();
            updateBalancesAndHistoryListView();
        } else {
            Alert invalidNameAlert = new Alert(Alert.AlertType.ERROR);
            invalidNameAlert.setTitle("Invalid input");
            invalidNameAlert.setHeaderText("Expense title should be unique and should consist only of " +
                    "alphanumeric characters and underscores.");
            invalidNameAlert.show();
        }
    }

    /**
     * Displays expense details (contributions) inside the
     * {@code optimalizationAndExpenseDetailsListView}
     */
    @FXML
    private void showExpenseDetails() {
        if (sceneryDisplayed.equals("history")) {
            optimalizationLabel.setText("Expense details");
            optimalizationAndExpenseDetailsListView.setVisible(true);
            int index = balanceAndHistoryListView.getSelectionModel().getSelectedIndex();
            if (index != -1) {
                Expense chosenExpense = expenseGroup.getExpenseList().
                        get(index);
                optimalizationAndExpenseDetailsListView.
                        setItems(FXCollections.observableArrayList(chosenExpense.getContributions().
                                keySet().stream().
                                map(user -> user.getName() + ": " + chosenExpense.getContributions().get(user) + " PLN."
                                ).collect(Collectors.toList())));

            }
        }
    }

    /**
     * Updates the {@code balanceAndHistoryListView} based on the value of
     * {@code sceneryDisplayed}.
     */
    private void updateBalancesAndHistoryListView() {
        viewOptimalization();
        switch (sceneryDisplayed) {
            case "history":
                balanceAndHistoryLabel.setText("Transaction history");
                balanceAndHistoryListView.setItems(FXCollections.observableArrayList(expenseGroup.getExpenseList().
                        stream().
                        map(e -> e.getName()).
                        collect(Collectors.toList())));
                break;
            case "balances":
                balanceAndHistoryLabel.setText("Users balances");
                balanceAndHistoryListView.setItems(FXCollections.observableArrayList(expenseGroup.getUsersBalancesMap().
                        keySet().stream().
                        map(user -> {
                            Double userBalance = expenseGroup.getUsersBalancesMap().get(user);
                            if (userBalance >= 0) {
                                return user.getName() + ": " + userBalance;
                            } else return user.getName() + ": " + userBalance;
                        }).collect(Collectors.toList())));
                break;
            case "total spendings":
                balanceAndHistoryLabel.setText("Users total spendings");
                balanceAndHistoryListView.setItems(FXCollections.observableArrayList(expenseGroup.getTotalUsersExpenseMap().
                        keySet().stream().
                        map(user -> {
                            Double userBalance = expenseGroup.getTotalUsersExpenseMap().get(user);
                            if (userBalance >= 0) {
                                return user.getName() + ": " + userBalance;
                            } else return user.getName() + ": " + userBalance;
                        }).collect(Collectors.toList())));
                break;
        }
    }


}
