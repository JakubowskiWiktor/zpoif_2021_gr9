package pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.database;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core.Expense;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core.ExpenseGroup;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core.User;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * A singleton used to interact with Firebase Realtime Database in context of
 * this app. Note: due to database rules, all database operations are
 * impossible to perform until signUp or signIn methods are called and
 * successfully executed.
 */
public class AppFirebaseManager extends DatabaseManager {

    /*
    Database structure (all keys are Strings):
    - userData:
        - <username>:
            - email: <email> (String)
            - expenseGroups: <expenseGroupsNames> (List<String>?)
    - groupData:
        - <expenseGroupName>:
            - usersBalancesMap:
                - <username>: <balance> (double)
                - ...
            - expenseList:
                - <expenseName>:
                    - contributions
                        - <username>: <contribution> (double)
                        - ...
                - ...
     */

    private final static AppFirebaseManager instance = new AppFirebaseManager();
    private final static String API_KEY = "AIzaSyD1nVZNCSYF_wN8EMeUR6Mrw-Hv2sNanBk";
    private final static HttpClient httpClient = HttpClient.newHttpClient();

    /**
     * An {@link FirebaseRestManager} object. In order to use its database
     * related methods, signUp or signIn function has to be called and
     * successfully executed first (to assign a value to authIdToken field).
     */
    private final FirebaseRestManager db =
            new FirebaseRestManager("zpoifproj1-default-rtdb", DatabaseRegion.EUROPE_WEST1, API_KEY);

    private AppFirebaseManager() {
    }

    public static AppFirebaseManager getInstance() {
        return instance;
    }

    public static HttpClient getHttpClient() {
        return httpClient;
    }

    @Override
    public User signUp(String email, String password) throws FirebaseHttpException {
        HttpResponse<String> response;
        JSONObject responseBody;
        try {
            response = db.processAuth(AuthMethod.SIGN_UP, email, password);
            responseBody = new JSONObject(response.body());
        } catch (URISyntaxException | InterruptedException | IOException err) {
            err.printStackTrace();
            return null;
        }
        if (response.statusCode() != 200) {
            throw new FirebaseHttpException(response);
        } else {
            db.setAuthIdToken(responseBody.getString("idToken"));
            String usernameFromEmail = generateUsernameFromEmail(email)
                    .replace(",", "");
            String defaultUsername = usernameFromEmail;
            int num = 1;
            while (getUser(defaultUsername) != null) {
                defaultUsername = usernameFromEmail + num;
                num++;
            }
            User user = new User(defaultUsername, email);
            storeUser(user);
            return user;
        }
    }

    @Override
    public User signIn(String email, String password) throws FirebaseHttpException {
        HttpResponse<String> response;
        JSONObject responseBody;
        try {
            response = db.processAuth(AuthMethod.SIGN_IN, email, password);
            responseBody = new JSONObject(response.body());
        } catch (URISyntaxException | IOException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
        if (response.statusCode() != 200) {
            throw new FirebaseHttpException(response);
        } else {
            db.setAuthIdToken(responseBody.getString("idToken"));
            return getAllUsers().stream()
                    .filter(user -> user.getMail().equals(email))
                    .findFirst()
                    .orElse(null);
        }
    }

    @Override
    public void signOut() {
        db.setAuthIdToken(null);
    }

    @Override
    public User getUser(String name) {
        HttpResponse<String> response = db.child("userData").child(name).get();
        if (response == null || response.body().equals("null")) {
            return null;
        }
        return parseUserFromJSON(name, new JSONObject(response.body()));
    }

    @Override
    public void storeUser(User user) {
        JSONObject dataToStore = new JSONObject();
        dataToStore.put("email", user.getMail());
        Set<String> userGroupsNames = user.getGroupsNames();
        JSONArray userGroupsNamesJSON = null;
        if (!userGroupsNames.isEmpty()) {
            userGroupsNamesJSON = new JSONArray(user.getGroupsNames());
        }
        dataToStore.put("expenseGroups", userGroupsNamesJSON);
        db.child("userData").child(user.getName()).update(dataToStore);
    }

    @Override
    public List<User> getAllUsers() {
        HttpResponse<String> response = db.child("userData").get();
        if (response == null || response.body().equals("null")) {
            return null;
        }
        JSONObject responseBody = new JSONObject(response.body());
        return responseBody.keySet().stream()
                .map(username -> parseUserFromJSON(
                        username, responseBody.getJSONObject(username)))
                .collect(Collectors.toList());
    }

    @Override
    public boolean updateUsername(User user, String newName) {
        String oldName = user.getName();
        newName = newName.replace(",", "");
        if (getUser(newName) != null || getUser(oldName) == null) {
            return false;
        }
        db.child("userData").child(oldName).delete();
        user.setName(newName);
        storeUser(user);

        // update names stored within groups
        FirebaseRestManager groupDataPointer = db.child("groupData");
        HttpResponse<String> rawGroupData = groupDataPointer.get();

        if (rawGroupData != null && !rawGroupData.body().equals("null")) {
            JSONObject groupData = new JSONObject(rawGroupData.body());
            String finalNewName = newName;

            groupData.keySet().forEach(groupName -> {
                // update balances maps
                JSONObject groupBalancesMap = groupData
                        .getJSONObject(groupName)
                        .getJSONObject("usersBalancesMap");
                FirebaseRestManager balancesPointer = groupDataPointer
                        .child(groupName)
                        .child("usersBalancesMap");
                if (groupBalancesMap.keySet().contains(oldName)) {
                    double balance = groupBalancesMap.getDouble(oldName);
                    balancesPointer.child(oldName).delete();
                    balancesPointer
                            .update(new JSONObject(String.format(
                                    "{%s: %f}", finalNewName, balance
                            )));
                }
                // update expenses
                JSONObject expenseList;
                try {
                    expenseList = groupData
                            .getJSONObject(groupName)
                            .getJSONObject("expenseList");
                } catch (JSONException err) {  // when group has no expenses
                    return;
                }
                FirebaseRestManager expensesPointer = groupDataPointer
                        .child(groupName)
                        .child("expenseList");
                expenseList.keySet().forEach(expenseName -> {
                    JSONObject contributions = expenseList
                            .getJSONObject(expenseName)
                            .getJSONObject("contributions");
                    if (contributions.keySet().contains(oldName)) {
                        double contribution = contributions.getDouble(oldName);
                        expensesPointer.child(expenseName)
                                .child("contributions")
                                .child(oldName).delete();
                        expensesPointer.child(expenseName)
                                .child("contributions")
                                .update(new JSONObject(String.format(
                                        "{%s: %f}", finalNewName, contribution
                                )));
                    }
                });
            });
        }
        return true;
    }

    @Override
    public ExpenseGroup getExpenseGroup(String groupName) {
        HttpResponse<String> response = db.child("groupData").child(groupName).get();
        if (response == null || response.body().equals("null")) {
            return null;
        }
        return parseExpenseGroupFromJSON(groupName, new JSONObject(response.body()));
    }

    @Override
    public void storeExpenseGroup(ExpenseGroup expenseGroup) {
        JSONObject dataToStore = new JSONObject();
        dataToStore.put("usersBalancesMap", expenseGroup.getUsersBalancesMap());

        JSONObject expenseList = new JSONObject();
        expenseGroup.getExpenseList().forEach(expense -> {
            JSONObject contributions = new JSONObject(expense.getContributions());
            expenseList.put(expense.getName(),
                    new JSONObject().put("contributions", contributions));
        });
        dataToStore.put("expenseList", expenseList);

        db.child("groupData").child(expenseGroup.getName()).update(dataToStore);
    }

    @Override
    public void deleteExpenseGroup(ExpenseGroup expenseGroup) {
        String groupName = expenseGroup.getName();
        db.child("groupData").child(groupName).delete();

        List<User> allUsers = getAllUsers();
        allUsers.forEach(user -> {
            if (user.getGroupsNames().contains(groupName)) {
                user.getGroupsNames().remove(groupName);
                if (user.getGroupsNames().isEmpty()) {
                    db.child("userData")
                            .child(user.getName())
                            .child("expenseGroups")
                            .delete();
                } else storeUser(user);
            }
        });
    }

    @Override
    public List<ExpenseGroup> getAllExpenseGroups() {
        HttpResponse<String> response = db.child("groupData").get();
        if (response == null || response.body().equals("null")) {
            return null;
        }
        JSONObject responseBody = new JSONObject(response.body());
        return responseBody.keySet().stream()
                .map(username -> parseExpenseGroupFromJSON(
                        username, responseBody.getJSONObject(username)))
                .collect(Collectors.toList());
    }

    /**
     * @param name user's name
     * @param data user's data stored in Firebase Database
     * @return A {@link User} object representing a user with provided data
     */
    private User parseUserFromJSON(String name, JSONObject data) {
        Set<String> expenseGroupsNames;
        try {
            expenseGroupsNames = data.getJSONArray("expenseGroups").toList()
                    .stream()
                    .map(Object::toString)
                    .collect(Collectors.toSet());
        } catch (JSONException err) {
            expenseGroupsNames = new HashSet<>();
        }
        return new User(name,
                data.getString("email"),
                expenseGroupsNames);
    }

    /**
     * @param name group's name
     * @param data group's data stored in Firebase Database
     * @return An {@link ExpenseGroup} object representing an expense group
     * with provided data
     */
    private ExpenseGroup parseExpenseGroupFromJSON(String name, JSONObject data) {
        Map<User, Double> usersBalancesMap =
                parseBalancesFromJSON(data.getJSONObject("usersBalancesMap"));
        List<Expense> expenseList;
        try {
            JSONObject rawExpensesData = data.getJSONObject("expenseList");
            expenseList = rawExpensesData.keySet().stream()
                    .map(expenseName -> parseExpenseFromJSON(
                            expenseName, rawExpensesData.getJSONObject(expenseName)))
                    .collect(Collectors.toList());
        } catch (JSONException err) {
            expenseList = new ArrayList<>();
        }
        return new ExpenseGroup(name, (HashMap<User, Double>) usersBalancesMap, expenseList);
    }

    /**
     * @param name expense's name
     * @param data expense's data stored in Firebase Database
     * @return An {@link Expense} object representing an expense group
     * with provided data
     */
    private Expense parseExpenseFromJSON(String name, JSONObject data) {
        return new Expense(name,
                parseBalancesFromJSON(data.getJSONObject("contributions")));
    }

    /**
     * @param data {@link JSONObject} containing usernames as keys and numbers
     *             as values
     * @return An {@link Expense} object representing an expense with
     * provided data
     */
    private Map<User, Double> parseBalancesFromJSON(JSONObject data) {
        Map<User, Double> usersBalancesMap = new HashMap<>();
        Map<String, Object> rawData = data.toMap();
        rawData.keySet().forEach(username ->
                usersBalancesMap.put(getUser(username),
                        Double.valueOf(rawData.get(username).toString())));
        return usersBalancesMap;
    }

    /**
     * A simple wrapper for HTTP requests to Firebase Realtime Database
     * and Firebase Auth.
     */
    private static class FirebaseRestManager {

        /**
         * An ID of the Firebase project
         */
        private final String projectId;

        /**
         * An ID Token obtained after successful authentication
         */
        private String authIdToken = null;

        private final DatabaseRegion databaseRegion;

        /**
         * Path to the child that the manager is pointing to
         */
        private Deque<String> childrenStack = new ArrayDeque<>();

        /**
         * API key of the firebase project
         */
        private final String apiKey;

        /**
         * This constructor should be used if the {@code authIdToken} is not known
         * at the time of creating an object, or when it is not necessary due
         * to the database rules. Note though that if the database rules
         * require authentication for certain actions, these actions will be
         * impossible to perform with this FirebaseRestManager, unless
         * {@code authIdToken} has been set after creation of an object using
         * {@code setAuthIdToken(String)} method.
         *
         * @param projectId      An ID of firebase project
         * @param databaseRegion Region where database is located
         * @param apiKey         API key of the firebase project
         */
        public FirebaseRestManager(String projectId, DatabaseRegion databaseRegion, String apiKey) {
            this.projectId = projectId;
            this.databaseRegion = databaseRegion;
            this.apiKey = apiKey;
        }

        /**
         * This constructor can be used if the {@code authIdToken} is known
         * at the time of creating an object.
         *
         * @param projectId      An ID of the firebase project
         * @param databaseRegion Region where database is located
         * @param authIdToken    An ID Token obtained after successful authentication
         * @param apiKey         API key of the firebase project
         */
        public FirebaseRestManager(String projectId, DatabaseRegion databaseRegion, String authIdToken, String apiKey) {
            this.projectId = projectId;
            this.databaseRegion = databaseRegion;
            this.authIdToken = authIdToken;
            this.apiKey = apiKey;
        }

        /**
         * This constructor is used only to enable chaining when using
         * {@code child(String)} and {@code prev()} methods.
         *
         * @param projectId      An ID of the firebase project
         * @param authIdToken    An ID Token obtained after successful authentication
         * @param databaseRegion Region where database is located
         * @param childrenStack  Path to the child which the manager is pointing to
         * @param apiKey         API key of the firebase project
         */
        private FirebaseRestManager(String projectId, String authIdToken, DatabaseRegion databaseRegion,
                                    Deque<String> childrenStack, String apiKey) {
            this.projectId = projectId;
            this.authIdToken = authIdToken;
            this.databaseRegion = databaseRegion;
            this.childrenStack = childrenStack;
            this.apiKey = apiKey;
        }

        /**
         * Wrapper for sign in and sign up requests
         *
         * @param authMethod specifies whether it is a sign-up or sign-in
         * @param email      user's email
         * @param password   user's password
         * @return Response from Firebase
         */
        public HttpResponse<String> processAuth(AuthMethod authMethod, String email, String password)
                throws IOException, InterruptedException, URISyntaxException {
            URI uri = null;
            switch (authMethod) {
                case SIGN_UP:
                    uri = new URI(
                            "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key="
                                    + API_KEY);
                    break;
                case SIGN_IN:
                    uri = new URI(
                            "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key="
                                    + API_KEY);
                    break;
            }
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(uri)
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(
                            new JSONObject()
                                    .put("email", email)
                                    .put("password", password)
                                    .put("returnSecureToken", true)
                                    .toString()
                    ))
                    .build();
            return httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        }

        /**
         * Returns a new object pointing to a different child in
         * the database. All the other attributes are preserved.
         *
         * @param path Path to a new child in the database. It is relative to
         *             the path specified by the manager calling that method
         * @return A new {@link FirebaseRestManager} pointing to a different
         * child
         */
        public FirebaseRestManager child(String... path) {
            Deque<String> newChildrenStack = new ArrayDeque<>(this.childrenStack);
            newChildrenStack.addAll(List.of(path));
            return new FirebaseRestManager(projectId, authIdToken, databaseRegion, newChildrenStack, apiKey);
        }

        /**
         * Pops the last element in children stack (going 1-step back in the
         * database tree). Like the {@code child()} method, it also returns a new
         * object instead of changing the current one.
         *
         * @return A new {@link FirebaseRestManager} object pointing to a
         * different child in the database
         */
        public FirebaseRestManager prev() {
            Deque<String> newChildrenStack = new ArrayDeque<>(childrenStack);
            newChildrenStack.pop();
            return new FirebaseRestManager(projectId, authIdToken, databaseRegion, newChildrenStack, apiKey);
        }

        public HttpResponse<String> get() {
            try {
                URI uri = formURI();
//                System.out.println("GET " + uri);
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(uri)
                        .GET()
                        .build();
                return httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            } catch (URISyntaxException | IOException | InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }

        public void update(JSONObject data) {
            try {
                URI uri = formURI();
//                System.out.println("UPDATE " + uri);
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(uri)
                        .header("X-HTTP-Method-Override", "PATCH")
                        .POST(HttpRequest.BodyPublishers.ofString(data.toString()))
                        .build();
                httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            } catch (URISyntaxException | IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void delete() {
            try {
                URI uri = formURI();
//                System.out.println("DELETE " + uri);
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(uri)
                        .DELETE()
                        .build();
                httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            } catch (URISyntaxException | IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }

        /**
         * Forms the URI used in requests.
         *
         * @return A {@link String} containing formed URI
         */
        private URI formURI() throws URISyntaxException {
            return new URI(String.format("https://" + databaseRegion.getDatabaseUrlScheme() + "/%s.json",
                    projectId, String.join("/", childrenStack)) +
                    (authIdToken == null ? "" : String.format("?auth=%s", authIdToken)));
        }

        public void setAuthIdToken(String authIdToken) {
            this.authIdToken = authIdToken;
        }
    }

    private enum DatabaseRegion {
        US_CENTRAL1("%s.firebaseio.com"),
        EUROPE_WEST1("%s.europe-west1.firebasedatabase.app"),
        ASIA_SOUTHEAST1("%s.asia-southeast1.firebasedatabase.app");

        private final String databaseUrlScheme;

        DatabaseRegion(String databaseUrlScheme) {
            this.databaseUrlScheme = databaseUrlScheme;
        }

        public String getDatabaseUrlScheme() {
            return databaseUrlScheme;
        }
    }

    private enum AuthMethod {SIGN_UP, SIGN_IN}
}
