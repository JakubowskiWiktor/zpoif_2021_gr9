package pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core;

import java.util.Map;

/**
 * A class representing a single expense of a group
 * @see ExpenseGroup
 * @see User
 */
public class Expense {

    /**
     * Name of the expense
     */
    private final String name;

    /**
     * Map of users' contributions in this expense
     */
    private final Map<User, Double> contributions;

    /**
     * A simple constructor initializing all the fields with values
     * passed as parameters
     */
    public Expense(String name, Map<User, Double> contributions) {
        this.name = name;
        this.contributions = contributions;
    }

    public String getName() {
        return name;
    }

    public Map<User, Double> getContributions() {
        return contributions;
    }
}
