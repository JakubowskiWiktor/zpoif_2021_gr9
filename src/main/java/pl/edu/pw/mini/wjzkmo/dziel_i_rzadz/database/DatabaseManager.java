package pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.database;

import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core.ExpenseGroup;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core.User;

import java.io.IOException;
import java.util.List;

/**
 * Represents a set of actions that can be performed with a database in
 * context of that app. It also contains authentication methods.
 *
 * @see User
 * @see ExpenseGroup
 */
public abstract class DatabaseManager {

    /**
     * Creates an account for a user in the database.
     *
     * @param email    user's email
     * @param password user's password
     * @return {@link User} object representing user account's data,
     * or {@code null} if an account with this email already exists
     */
    public abstract User signUp(String email, String password) throws IOException;

    /**
     * Signs user into the database.
     *
     * @param email    user's email
     * @param password user's password
     * @return {@link User} object representing user account's data,
     * or {@code null} if credentials were incorrect
     */
    public abstract User signIn(String email, String password) throws IOException;

    /**
     * Signs user out of the database.
     */
    public abstract void signOut();

    /**
     * Reads data about a user with a given name.
     *
     * @param name String containing user's name
     * @return {@link User} object representing a user with a given name, or
     * {@code null} if such a user does not exist in the database
     */
    public abstract User getUser(String name);

    /**
     * Stores data about given user.
     *
     * @param user {@link User} class object representing a user whose data
     *             is to be stored
     */
    public abstract void storeUser(User user);

    /**
     * Reads data about all the users in the database
     *
     * @return A collection of {@link User} class objects representing users'
     * data
     */
    public abstract List<User> getAllUsers();

    /**
     * Updates name of a user
     *
     * @param user    User whose name is to be changed
     * @param newName new username
     * @return {@code true} if user's name has been successfully changed, or
     * {@code false} if new name is already taken or if such user doesn't
     * exist in the database.
     */
    public abstract boolean updateUsername(User user, String newName);

    /**
     * Reads data about an expense group with a given name.
     *
     * @param groupName String containing group's name
     * @return {@link ExpenseGroup} object representing an expense group with a given name, or
     * {@code null} if such a group does not exist in the database
     */
    public abstract ExpenseGroup getExpenseGroup(String groupName);

    /**
     * Stores data about given expense group.
     *
     * @param expenseGroup {@link ExpenseGroup} class object representing a group which data
     *                     is to be stored
     */
    public abstract void storeExpenseGroup(ExpenseGroup expenseGroup);

    /**
     * Safely deletes specified expense group from database.
     *
     * @param expenseGroup group to be deleted
     */
    public abstract void deleteExpenseGroup(ExpenseGroup expenseGroup);

    /**
     * Reads data about all the groups in the database
     *
     * @return A collection of {@link ExpenseGroup} class objects representing
     * groups' data
     */
    public abstract List<ExpenseGroup> getAllExpenseGroups();

    protected static String generateUsernameFromEmail(String email) {
        return email.split("@")[0].split("\\.")[0];
    }
}
