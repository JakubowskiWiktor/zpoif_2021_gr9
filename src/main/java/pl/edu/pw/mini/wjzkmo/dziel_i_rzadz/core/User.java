package pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A class representing users using the application
 * @see ExpenseGroup
 */
public class User {

    /**
     * Username
     */
    private String name;

    /**
     * User's email
     */
    private final String mail;

    /**
     * List of names of expense groups which user belongs to
     */
    private final Set<String> expenseGroupsNames;

    /**
     * Constructor used when reading existing users from the database.
     */
    public User(String name, String mail, Set<String> expenseGroupsNames) {
        this.name = name;
        this.mail = mail;
        this.expenseGroupsNames = expenseGroupsNames;
    }

    /**
     * Constructor used when a new user has created his account.
     * It defaults {@code expenseGroupNames} to an empty HashSet.
     */
    public User(String name, String mail) {
        this.name = name;
        this.mail = mail;
        expenseGroupsNames = new HashSet<>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public Set<String> getGroupsNames() {
        return expenseGroupsNames;
    }

    /**
     * Two users are equal when their names are equal
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        User user = (User) obj;

        return Objects.equals(name, user.name);
    }

    /**
     * User's hashcode is the same as its name's hashcode, or
     * 0 if {@code name} is {@code null}.
     */
    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
