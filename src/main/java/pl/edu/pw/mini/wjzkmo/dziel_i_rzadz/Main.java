package pl.edu.pw.mini.wjzkmo.dziel_i_rzadz;

/**
 * Main runnable class for .jar
 */
public class Main {

    public static void main(String[] args) {
        DivideAndConquerApplication.main(args);
    }
}
