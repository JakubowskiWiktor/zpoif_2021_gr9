package pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core.User;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.database.AppFirebaseManager;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.database.DatabaseManager;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.database.FirebaseHttpException;

import java.io.IOException;

/**
 * A controller for authentication screen
 *
 * @see User
 * @see DatabaseManager
 * @see AppFirebaseManager
 */
public class AuthScreenController {

    /**
     * An instance of {@link AppFirebaseManager} used to interact
     * with the database
     */
    private final DatabaseManager database = AppFirebaseManager.getInstance();

    /**
     * A text field where user can input his email when signing in/up
     */
    @FXML
    private TextField emailTextField;

    /**
     * A text field where user can input his password when signing in/up
     */
    @FXML
    private PasswordField passwordField;

    /**
     * A label used to display messages of errors that might occur
     * when signing in/up
     */
    @FXML
    private Label errorLabel;

    /**
     * Handles signing in, assigned to sign in button
     */
    @FXML
    protected void signIn() {
        String email = emailTextField.getText();
        String password = passwordField.getText();
        User user = null;
        try {
            user = database.signIn(email, password);
        } catch (FirebaseHttpException e) {
            switch (e.getErrorType()) {
                case INVALID_EMAIL:
                    errorLabel.setText("Invalid email");
                    break;
                case MISSING_PASSWORD:
                    errorLabel.setText("Missing password");
                    break;
                case USER_DISABLED:
                    errorLabel.setText("This user has been disabled");
                    break;
                case EMAIL_NOT_FOUND:
                    errorLabel.setText("A user with such email does not exist");
                    break;
                case INVALID_PASSWORD:
                    errorLabel.setText("Invalid password");
                    break;
                case TOO_MANY_ATTEMPTS_TRY_LATER:
                    errorLabel.setText("Too many attempts! Please try again later");
                    break;
                case UNKNOWN_ERROR:
                    String errorInfo = e.getErrorType().toString();
                    if (!errorInfo.equals(e.getErrorDescription())) {
                        errorInfo += " " + e.getErrorDescription();
                    }
                    errorLabel.setText("An unknown error occurred:\n" + errorInfo);
            }
            return;
        } catch (IOException ignored) {
        }
        if (user == null) {
            errorLabel.setText("An unknown error occurred");
        } else {
            moveToMainMenu(user);
        }
    }

    /**
     * Handles signing up, assigned to sign up button
     */
    @FXML
    protected void signUp() {
        User user = null;
        try {
            user = database.signUp(emailTextField.getText(), passwordField.getText());
        } catch (FirebaseHttpException e) {
            switch (e.getErrorType()) {
                case INVALID_EMAIL:
                    errorLabel.setText("Invalid email");
                    break;
                case MISSING_PASSWORD:
                    errorLabel.setText("Missing password");
                    break;
                case TOO_MANY_ATTEMPTS_TRY_LATER:
                    errorLabel.setText("Too many attempts! Please try again later");
                    break;
                case EMAIL_EXISTS:
                    errorLabel.setText("This email is already taken");
                    break;
                case WEAK_PASSWORD:
                    errorLabel.setText(e.getErrorDescription());
                    break;
                case UNKNOWN_ERROR:
                    String errorInfo = e.getErrorType().toString();
                    if (!errorInfo.equals(e.getErrorDescription())) {
                        errorInfo += "\n" + e.getErrorDescription();
                    }
                    errorLabel.setText("An unknown error occurred:\n" + errorInfo);
            }
            return;
        } catch (IOException ignored) {
        }
        if (user == null) {
            errorLabel.setText("An unknown error occurred");
        } else {
            moveToMainMenu(user);
        }
    }

    /**
     * Switches screen to main menu
     *
     * @param user A {@link User} object representing a user that
     *             has just signed in/up
     * @see MainMenuController
     */
    @FXML
    private void moveToMainMenu(User user) {
        Stage stage = (Stage) (emailTextField.getScene().getWindow());
        FXMLLoader loader = new FXMLLoader(getClass()
                .getResource("/pl/edu/pw/mini/wjzkmo/dziel_i_rzadz/main-menu.fxml"));
        try {
            AnchorPane mainMenu = loader.load();
            MainMenuController controller = loader.getController();
            controller.setUser(user);
            stage.setScene(new Scene(mainMenu));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
