package pl.edu.pw.mini.wjzkmo.dziel_i_rzadz;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Main Application class
 */
public class DivideAndConquerApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(DivideAndConquerApplication.class.getResource("auth-screen.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 684, 360);
        stage.setTitle("Divide and conquer");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}