package pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core;

import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.database.AppFirebaseManager;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A class representing an expense group
 *
 * @see User
 * @see Expense
 */
public class ExpenseGroup {

    /**
     * Name of the group
     */
    private String name;

    /**
     * A Map of balances (what user paid minus what other users
     * have paid for him/her).
     */
    private HashMap<User, Double> usersBalancesMap;

    /**
     * A map of total expenses (how much users have in total)
     */
    private HashMap<User, Double> totalUsersExpenseMap;

    /**
     * List of {@link Expense} objects representing expenses in this group
     */
    private final List<Expense> expenseList;

    public void setUsersBalancesMap(HashMap<User, Double> usersBalancesMap) {
        this.usersBalancesMap = usersBalancesMap;
    }

    /**
     * Constructor used when reading group from database
     */
    public ExpenseGroup(String name, HashMap<User, Double> usersBalancesMap, List<Expense> expenseList) {
        this.name = name;
        this.usersBalancesMap = usersBalancesMap;
        this.expenseList = expenseList;
        totalUsersExpenseMap = new HashMap<>();
        usersBalancesMap.keySet().forEach(user -> totalUsersExpenseMap.put(user, 0.0));
        expenseList.forEach(expense -> {
            Map<User, Double> contributions = expense.getContributions();
            contributions.keySet()
                    .forEach(user -> totalUsersExpenseMap.put(user,
                            totalUsersExpenseMap.get(user) +
                                    contributions.get(user)));
        });
    }

    /**
     * Constructor used when creating new group from gui.<br>
     * The fields {@code expenseList} and {@code totalUsersExpenseMap} are
     * assigned empty collections as default values ({@code ArrayList} and
     * {@code HashMap} respectively).
     */
    public ExpenseGroup(String name, List<User> usersList) {
        this.name = name;
        this.usersBalancesMap = new HashMap<>();
        usersList.forEach(u -> {
            usersBalancesMap.put(u, 0.0);
            u.getGroupsNames().add(name);
            AppFirebaseManager.getInstance().storeUser(u);
        });
        this.expenseList = new ArrayList<>();
        totalUsersExpenseMap = new HashMap<>();
        usersBalancesMap.keySet().forEach(user -> totalUsersExpenseMap.put(user, 0.0));
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public HashMap<User, Double> getUsersBalancesMap() {
        return usersBalancesMap;
    }

    public HashMap<User, Double> getTotalUsersExpenseMap() {
        return totalUsersExpenseMap;
    }

    /**
     * Returns a collection of members of this expense group.
     *
     * @return A {@link List} of {@link User} objects representing group
     * members.
     */
    public List<User> getUsers() {
        return new ArrayList<>(usersBalancesMap.keySet());
    }

    /**
     * Returns a collection of names of this group's members.
     *
     * @return A {@link List} of users' names.
     */
    public List<String> getUsersNames() {
        return usersBalancesMap.keySet().stream().map(User::getName).collect(Collectors.toList());
    }

    public List<Expense> getExpenseList() {
        return expenseList;
    }

    /**
     * Two groups are equal when their names are equal
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        ExpenseGroup expenseGroup = (ExpenseGroup) obj;

        return Objects.equals(name, expenseGroup.name);
    }

    /**
     * Group's hashcode is the same as its name's hashcode, or
     * 0 if {@code name} is {@code null}.
     */
    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "ExpenseGroup{" +
                "\n\tname='" + name + '\'' +
                ",\n\tusersBalancesMap=" + usersBalancesMap +
                ",\n\ttotalUsersExpenseMap=" + totalUsersExpenseMap +
                ",\n\texpenseList=" + expenseList +
                "\n}";
    }
}
