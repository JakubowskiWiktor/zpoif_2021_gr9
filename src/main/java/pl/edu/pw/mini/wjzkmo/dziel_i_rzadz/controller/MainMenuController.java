package pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core.ExpenseGroup;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.core.User;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.database.AppFirebaseManager;
import pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.database.DatabaseManager;

import java.io.IOException;
import java.util.*;

/**
 * A controller for main menu screen
 *
 * @see User
 * @see ExpenseGroup
 * @see DatabaseManager
 * @see AppFirebaseManager
 */
public class MainMenuController {

    /**
     * An instance of {@link AppFirebaseManager} used to interact
     * with the database
     */
    private final DatabaseManager database = AppFirebaseManager.getInstance();

    private Stage stage;
    private Scene scene;
    private Parent root;

    /**
     * A {@link User} object representing the user that is viewing this screen
     */
    private User user;

    /**
     * A label displaying logged user's name
     */
    @FXML
    private Label usernameLabel;

    /**
     * A label displaying a welcome text as well as tooltips when
     * mouse is hovering above other components on the screen
     */
    @FXML
    private Label welcomeText;

    /**
     * A text field where user can input the name of the group
     * he/she wants to create
     */
    @FXML
    private TextField createGroupName;

    /**
     * A ListView displaying names of users selected to be added to a new group
     */
    @FXML
    private ListView<User> selectUsersListView;

    /**
     * A ComboBox that lets user pick a name of a user that he/she wants to
     * add to a new group
     */
    @FXML
    private ComboBox<User> selectUsersComboBox;

    /**
     * A ComboBox that lets user pick a group name which he wants to view
     */
    @FXML
    private ComboBox<String> selectGroupComboBox;

    /**
     * A list of users that were selected to be added to a new group
     */
    private List<User> newGroupMembers = new ArrayList<>();

    /**
     * Initialises {@code user} field, welcome text as well as ComboBoxes' items
     *
     * @param user value to be assigned to {@code} user field
     */
    public void setUser(User user) {
        this.user = database.getUser(user.getName());  // to get a fresh version of this user
        refreshUsernameLabel();
        welcomeText.setText("Welcome " + user.getName());
    }

    /**
     * Updates {@code selectGroupComboBox} items
     */
    @FXML
    protected void updateSelectGroupComboBox() {
        selectGroupComboBox.setItems(FXCollections.observableArrayList(database.getUser(user.getName()).getGroupsNames()));
    }

    /**
     * Updates {@code selectUsersComboBox} items
     */
    @FXML
    protected void updateSelectUsersComboBox() {
        selectUsersComboBox.setItems(FXCollections.observableArrayList(database.getAllUsers()));
    }

    /**
     * Handles the creation of a new group
     */
    @FXML
    protected void createGroup(ActionEvent event) throws IOException {
        String groupName = createGroupName.getText();
        if (!groupName.matches("[A-Za-z0-9_ąężźćółńś]+")) {  // invalid name
            Alert invalidNameAlert = new Alert(Alert.AlertType.ERROR);
            invalidNameAlert.setTitle("Invalid group name");
            invalidNameAlert.setHeaderText("Group name should consist only of " +
                    "alphanumeric characters and underscores.");
            invalidNameAlert.show();
            return;
        }
        if (database.getExpenseGroup(groupName) != null) {
            Alert invalidGroupNameAlert = new Alert(Alert.AlertType.ERROR);
            invalidGroupNameAlert.setTitle("Group already exists");
            invalidGroupNameAlert.setHeaderText("Expense group by the name of " + groupName + " already exists. " +
                    "Please select other name.");
            invalidGroupNameAlert.show();
        } else if (!newGroupMembers.isEmpty()) {
            ExpenseGroup group = new ExpenseGroup(groupName, newGroupMembers);
            database.storeExpenseGroup(group);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/pl/edu/pw/mini/wjzkmo/dziel_i_rzadz/group-menu.fxml"));
            root = loader.load();
            GroupMenuController groupMenuController = loader.getController();
            groupMenuController.setParameters(group, user);
            groupMenuController.setGroupName(group.getName());
            stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }

    }

    /**
     * Invoked when the user picks a user to be added to a new group
     */
    @FXML
    protected void addUser() {
        User addedUser = selectUsersComboBox.getValue();
        if (addedUser != null) {
            newGroupMembers.add(addedUser);
            selectUsersListView.setItems(FXCollections.observableArrayList(newGroupMembers));
        }

    }

    /**
     * Switches screen to group menu. It does nothing if no group
     * has been selected in the {@code selectGroupComboBox}
     *
     * @see GroupMenuController
     */
    @FXML
    protected void goToSelectedGroup(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/pl/edu/pw/mini/wjzkmo/dziel_i_rzadz/group-menu.fxml"));
        root = loader.load();
        GroupMenuController groupMenuController = loader.getController();
        if (selectGroupComboBox.getValue() != null) {
            ExpenseGroup group = database.getExpenseGroup(selectGroupComboBox.getValue());
            groupMenuController.setParameters(group, user);
            groupMenuController.setGroupName(group.getName());
            stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }
    }

    /**
     * Updates the name of the user that is currently logged in.
     * Invoked when the "Change username" button is clicked.
     */
    @FXML
    protected void changeUsername() {
        String oldName = user.getName();
        TextInputDialog newNameInputDialog = new TextInputDialog(oldName);
        newNameInputDialog.setHeaderText("New username:");
        newNameInputDialog.showAndWait();
        String newName = newNameInputDialog
                .getEditor().getText();

        if (!newName.matches("[A-Za-z0-9_ąężźćółńś]+")) {  // invalid name
            Alert invalidNameAlert = new Alert(Alert.AlertType.ERROR);
            invalidNameAlert.setTitle("Invalid username");
            invalidNameAlert.setHeaderText("Username should consist only of " +
                    "alphanumeric characters and underscores");
            invalidNameAlert.show();
            return;
        }

        AppFirebaseManager.getInstance().updateUsername(user, newName);
        refreshUsernameLabel();
    }

    /**
     * Sets the text displayed by {@code welcomeText} to the
     * standard welcome text
     */
    @FXML
    protected void setTextToWelcome(MouseEvent e) {
        welcomeText.setText("Welcome " + user.getName());
    }

    /**
     * Sets the text displayed by {@code welcomeText} to the
     * instruction on selecting users to add to a new group
     */
    @FXML
    protected void setTextToEnterUsernamesInstruction(MouseEvent e) {
        welcomeText.setText("Select your friend or your's username, then click 'add user' button");
    }

    /**
     * Sets the text displayed by {@code welcomeText} to the
     * instruction on typing a new group name
     */
    @FXML
    protected void setTextToGroupnameInstruction(MouseEvent e) {
        welcomeText.setText("Enter the name of new group");
    }

    /**
     * Sets the text displayed by {@code welcomeText} to the
     * instruction on finalising new group's creation
     */
    @FXML
    protected void setTextToCreateGroupInstruction(MouseEvent e) {
        welcomeText.setText("After creating group members list, click to create your new group");
    }

    /**
     * Sets the text displayed by {@code welcomeText} to the
     * information about {@code selectUsersListView}
     */
    @FXML
    protected void setTextToDisplayUserNamesInstruction(MouseEvent e) {
        welcomeText.setText("This is the list of users, that will create your new group");
    }

    /**
     * Sets the text displayed by {@code welcomeText} to the
     * instruction on selecting the group to view details of
     */
    @FXML
    protected void setTextToChooseGroupInstruction(MouseEvent e) {
        welcomeText.setText("Click to select expense group you want to display");
    }

    /**
     * Sets the text displayed by {@code welcomeText} to the
     * instruction on how to view the selected group's details
     */
    @FXML
    protected void setTextToSelectGroupInstruction(MouseEvent e) {
        welcomeText.setText("Click display selected expense group's details");
    }

    /**
     * Logs user out and switches back to authentication screen
     *
     * @see AuthScreenController
     */
    @FXML
    protected void logout(ActionEvent event) throws IOException {
        database.signOut();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/pl/edu/pw/mini/wjzkmo/dziel_i_rzadz/auth-screen.fxml"));
        root = loader.load();
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Refreshes the {@code usernameLabel}.
     */
    private void refreshUsernameLabel() {
        usernameLabel.setText("Logged in as " + user.getName());
    }
}