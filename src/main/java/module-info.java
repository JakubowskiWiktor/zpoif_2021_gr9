module pl.edu.pw.mini.wjzkmo.dziel_i_rzadz {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.json;
    requires java.net.http;


    opens pl.edu.pw.mini.wjzkmo.dziel_i_rzadz to javafx.fxml;
    opens pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.controller to javafx.fxml;
    exports pl.edu.pw.mini.wjzkmo.dziel_i_rzadz;
    exports pl.edu.pw.mini.wjzkmo.dziel_i_rzadz.controller;
}